import { Flex, Paper } from '@mantine/core';
import { Outlet } from 'react-router-dom';

import { Logo } from '~/components/Logo';

export function AuthLayout() {
  return (
    <Flex w="100%" h="100vh" align="center" justify="center">
      <Paper shadow="xl" withBorder p="xl" w={400}>
        <Logo />
        <Outlet />
      </Paper>
    </Flex>
  );
}
