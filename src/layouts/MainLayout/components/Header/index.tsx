import { Avatar, Box, Container, Flex, Menu, rem, Text } from '@mantine/core';
import { IconChevronDown, IconCloud } from '@tabler/icons-react';
import { FC } from 'react';

import { useUser } from '~/shared/store/user';

interface HeaderProps {}

export const Header: FC<HeaderProps> = () => {
  const { removeUser } = useUser();
  const handleLogout = () => {
    removeUser();
  };

  return (
    <Container size="lg" h="100%">
      <Flex h="100%" gap="md" align="center" wrap="wrap">
        <Flex align="center" justify="center">
          <Avatar variant="filled" radius="md" size="3rem" color="white">
            <IconCloud
              style={{ width: rem(30), height: rem(30) }}
              color="var(--mantine-color-blue-filled)"
            />
          </Avatar>
          <Text tt="uppercase" fw="bold" visibleFrom="xs">
            Weather App
          </Text>
        </Flex>
        <Box ml="auto">
          <Menu position="bottom-end">
            <Menu.Target>
              <Flex
                align="center"
                gap="0.2rem"
                style={{
                  cursor: 'pointer',
                }}
              >
                <Text fw={500}>yaroslavert02@gmail.com</Text>
                <IconChevronDown size="1.2rem" stroke={1.5} />
              </Flex>
            </Menu.Target>
            <Menu.Dropdown>
              <Menu.Item onClick={handleLogout}>Log Out</Menu.Item>
            </Menu.Dropdown>
          </Menu>
        </Box>
      </Flex>
    </Container>
  );
};
