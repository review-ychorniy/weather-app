import { AppShell } from '@mantine/core';
import { Outlet } from 'react-router-dom';

import { Header } from './components/Header';

export function MainLayout() {
  return (
    <AppShell
      display="flex"
      header={{ height: { base: 48, sm: 60, lg: 80 } }}
      style={{ flexDirection: 'column', minHeight: '100vh' }}
      bg="ligthBlue"
    >
      <AppShell.Header>
        <Header />
      </AppShell.Header>
      <AppShell.Main flex={1} mih="auto" display="flex">
        <Outlet />
      </AppShell.Main>
    </AppShell>
  );
}
