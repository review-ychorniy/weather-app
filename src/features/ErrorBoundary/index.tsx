import { Button, Collapse, Container, Flex, Paper, Text } from '@mantine/core';
import { IconChevronDown, IconChevronRight } from '@tabler/icons-react';
import { Component, ErrorInfo, ReactNode } from 'react';

export interface ErrorBoundaryProps {
  showDetails?: boolean;
  children: ReactNode;
  title?: string;
  message?: string;
}

interface State {
  hasError: boolean;
  error: Error | null;
  errorInfo: ErrorInfo | null;
  opened: boolean;
}

class ErrorBoundary extends Component<ErrorBoundaryProps, State> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false, error: null, errorInfo: null, opened: false };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.setState({
      hasError: true,
      error,
      errorInfo,
    });
    // eslint-disable-next-line no-console
    console.error('Uncaught error:', error, errorInfo);
  }

  handleToggle = () => {
    this.setState((state) => ({ opened: !state.opened }));
  };

  render() {
    const {
      state: { hasError, error, errorInfo, opened },
      props: { title = 'Something went wrong', message = 'Error', children, showDetails },
      handleToggle,
    } = this;

    if (hasError) {
      return (
        <Container size="lg" h="100%" my="lg">
          <Flex gap="nano" direction="column" align="baseline">
            <Text component="h3" fz="lg" fw="bold">
              {title}
            </Text>
            {showDetails && (
              <>
                <Flex gap="nano" align="center">
                  <Text c="red" fw="bold" fz="lg">
                    {message}
                  </Text>
                  <Text c="dark" fw="bold">
                    {error?.message}
                  </Text>
                </Flex>
                <Button
                  leftSection={
                    opened ? <IconChevronDown size="1.2rem" /> : <IconChevronRight size="1.2rem" />
                  }
                  onClick={handleToggle}
                >
                  Details
                </Button>
                <Collapse in={opened}>
                  <Paper shadow="lg" radius="md" withBorder mt="xs" p="lg">
                    <Text c="dark" fw="bold">
                      {errorInfo?.componentStack}
                    </Text>
                  </Paper>
                </Collapse>
              </>
            )}
          </Flex>
        </Container>
      );
    }

    return children;
  }
}

export { ErrorBoundary };
