export type DayForecast = {
  date: string;
  morning: number;
  afternoon: number;
  evening: number;
  night: number;
};
