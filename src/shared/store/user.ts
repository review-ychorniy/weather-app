import { create } from 'zustand';
import { persist } from 'zustand/middleware';

import { User } from '../types/user';

type State = {
  user: User | null;
  setUser: (user: User) => void;
  removeUser: () => void;
};

export const useUser = create<State>()(
  persist(
    (set) => ({
      user: null,
      setUser: (user: User) => {
        set({ user });
      },
      removeUser: () => {
        set({ user: null });
      },
    }),
    {
      name: 'user-storage', // name of the item in the storage (must be unique)
    },
  ),
);
