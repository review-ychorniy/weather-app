import { create } from 'zustand';

import { DayForecast } from '../types/forecast';

type State = {
  forecasts: DayForecast[];
  setForecasts: (forecasts: DayForecast[]) => void;
  removeForecasts: () => void;
};

export const useForecast = create<State>()((set) => ({
  forecasts: [],
  setForecasts: (forecasts: DayForecast[]) => {
    set({ forecasts });
  },
  removeForecasts: () => {
    set({ forecasts: [] });
  },
}));
