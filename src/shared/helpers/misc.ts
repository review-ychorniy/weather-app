export function sleap(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function randomNumber(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
