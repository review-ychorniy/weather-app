import { sleap } from '~/shared/helpers/misc';

export async function signUpFetch(email: string) {
  await sleap(300);
  return Promise.resolve({
    email,
  });
}
