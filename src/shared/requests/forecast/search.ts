import dayjs from 'dayjs';

import { randomNumber, sleap } from '~/shared/helpers/misc';
import { DayForecast } from '~/shared/types/forecast';

function generateDateForecast(date: string): DayForecast {
  return {
    date: date,
    morning: randomNumber(4, 11),
    afternoon: randomNumber(10, 25),
    evening: randomNumber(10, 20),
    night: randomNumber(0, 5),
  };
}

export async function searchForecast(city: string) {
  await sleap(300);
  return Promise.resolve({
    city,
    forecast: [
      generateDateForecast(dayjs().startOf('day').format()),
      generateDateForecast(dayjs().startOf('day').add(1, 'day').format()),
    ],
  });
}
