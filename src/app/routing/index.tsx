import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import { AuthLayout } from '~/layouts/AuthLayout';
import { MainLayout } from '~/layouts/MainLayout';
import { SignUpPage } from '~/pages/Auth/SignUp';
import { HomePage } from '~/pages/Home';
import { NotFoundPage } from '~/pages/NotFound';

import { ROUTES } from './consts';
import { RequireAuth } from './require-auth';

export function Routing() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={ROUTES.HOME.PATH} element={<MainLayout />}>
          <Route
            index
            element={
              <RequireAuth>
                <HomePage />
              </RequireAuth>
            }
          />
        </Route>
        <Route path={ROUTES.AUTH.PATH} element={<AuthLayout />}>
          <Route index element={<Navigate to={ROUTES.AUTH.SIGN_UP.PATH}></Navigate>}></Route>
          <Route path={ROUTES.AUTH.SIGN_UP.PATH} element={<SignUpPage />} />
        </Route>
        <Route path="*" element={<NotFoundPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
