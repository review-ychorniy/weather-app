import { Navigate, useLocation } from 'react-router-dom';

import { useUser } from '~/shared/store/user';

import { ROUTES } from './consts';

export function RequireAuth({ children }: { children: JSX.Element }) {
  const { user } = useUser();
  const location = useLocation();

  if (!user) {
    return <Navigate to={ROUTES.AUTH.SIGN_UP.PATH} state={{ from: location }} replace />;
  }

  return children;
}
