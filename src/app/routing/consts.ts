export const ROUTES = {
  HOME: {
    PATH: '/',
  },
  AUTH: {
    PATH: '/auth',
    SIGN_UP: {
      PATH: '/auth/sign-up',
    },
  },
  NOT_FOUND: {
    PATH: '/*',
  },
  FORBIDDEN: {
    PATH: '/403',
  },
};
