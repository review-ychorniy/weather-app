import '@mantine/core/styles.css';
import '@mantine/dates/styles.css';
import './App.scss';

import { MantineProvider } from '@mantine/core';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import { ErrorBoundary } from '~/features/ErrorBoundary';

import { Routing } from './routing';

const queryClient = new QueryClient();

function App() {
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <MantineProvider>
          <ErrorBoundary showDetails={import.meta.env.DEV}>
            <Routing />
          </ErrorBoundary>
        </MantineProvider>
      </QueryClientProvider>
    </>
  );
}

export default App;
