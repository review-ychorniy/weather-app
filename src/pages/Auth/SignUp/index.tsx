import { Box, Button, Flex, LoadingOverlay, Text, TextInput } from '@mantine/core';
import { useMutation } from '@tanstack/react-query';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import * as yup from 'yup';

import { ROUTES } from '~/app/routing/consts';
import { signUpFetch } from '~/shared/requests/auth/sign-up';
import { useUser } from '~/shared/store/user';

const schema = yup.object().shape({
  email: yup.string().required('Invalid email').email('Invalid email'),
});

export function SignUpPage() {
  const { setUser } = useUser();
  const navigate = useNavigate();
  const mutation = useMutation({
    mutationFn: signUpFetch,
    onSuccess: (data) => {
      setUser(data);
      navigate(ROUTES.HOME.PATH);
    },
  });

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: schema,
    onSubmit: (values) => {
      mutation.mutate(values.email);
    },
  });

  const { values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting } = formik;

  return (
    <Box>
      <LoadingOverlay visible={mutation.isPending} />
      <Text fw="bold" mb="0" fz="h3" ta="center">
        Create new Acount
      </Text>
      <Box component="form" onSubmit={handleSubmit} noValidate>
        <Box mt="xs">
          <TextInput
            type="email"
            name="email"
            label="Email"
            placeholder="supper@example.com"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
            error={errors.email && touched.email && errors.email}
          />
        </Box>
        <Flex mt="xs" align="center" justify="center">
          <Button type="submit" mt="sm" disabled={isSubmitting}>
            Sign Up
          </Button>
        </Flex>
      </Box>
    </Box>
  );
}
