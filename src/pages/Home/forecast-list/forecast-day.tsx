import { SimpleGrid, Stack, Text } from '@mantine/core';
import dayjs from 'dayjs';
import isToday from 'dayjs/plugin/isToday';
import { useMemo } from 'react';

import { DayForecast } from '~/shared/types/forecast';

import { ForecasPeriod } from './forecast-period';
import { CloudIcon, CloudSunIcon, MoonStarIcon, SunIcon } from './icons';

dayjs.extend(isToday);

type Props = {
  forecast: DayForecast;
};

export function ForecastDay({ forecast }: Props) {
  const date = dayjs(forecast.date);
  const isToday = date.isToday();

  const canShowPeriods = useMemo(() => {
    const currentHour = dayjs().get('hour');
    const list = {
      morning: true,
      afternoon: true,
      evening: true,
      night: true,
    };

    if (isToday) {
      list.morning = currentHour <= 12;
      list.afternoon = currentHour <= 18;
      list.evening = currentHour <= 21;
      list.night = currentHour <= 24;
    }

    return list;
  }, [isToday]);

  return (
    <Stack>
      <Text>Date {date.format('DD MMM')}</Text>
      <SimpleGrid cols={{ base: 1, sm: 2, lg: 4 }}>
        {canShowPeriods.morning && (
          <ForecasPeriod
            name="morming"
            tempareture={forecast.morning}
            icon={<CloudSunIcon color="var(--mantine-color-yellow-5)" />}
          />
        )}
        {canShowPeriods.afternoon && (
          <ForecasPeriod
            name="afternoon"
            tempareture={forecast.afternoon}
            icon={<SunIcon color="var(--mantine-color-orange-6)" />}
          />
        )}
        {canShowPeriods.afternoon && (
          <ForecasPeriod
            name="evening"
            tempareture={forecast.evening}
            icon={<CloudIcon color="var(--mantine-color-blue-3)" />}
          />
        )}
        {canShowPeriods.afternoon && (
          <ForecasPeriod
            name="night"
            tempareture={forecast.night}
            icon={<MoonStarIcon color="var(--mantine-color-blue-9)" />}
          />
        )}
      </SimpleGrid>
    </Stack>
  );
}
