import { Flex, Paper, Text } from '@mantine/core';
import { IconTemperature } from '@tabler/icons-react';
import { ReactNode } from 'react';

type Props = {
  name: string;
  tempareture: number;
  icon: ReactNode;
};

export function ForecasPeriod({ name, tempareture, icon }: Props) {
  return (
    <Paper shadow="xl" withBorder px="lg" py="lg" radius="sm">
      <Flex gap="xs" color="red">
        <Text fz="h4" tt="capitalize" fw="bold">
          {name}
        </Text>
        {icon}
      </Flex>
      <Flex mt="xs" gap="xs">
        <Text>
          <IconTemperature color="gray" />
        </Text>
        <Text fw="bold" fz="md">
          {tempareture}°C
        </Text>
      </Flex>
    </Paper>
  );
}
