import { DayForecast } from '~/shared/types/forecast';

import { ForecastDay } from './forecast-day';

type Props = {
  forecasts: DayForecast[];
};

export function ForecastList({ forecasts }: Props) {
  return forecasts.map((forecast) => <ForecastDay key={forecast.date} forecast={forecast} />);
}
