import { Flex, Loader, Stack, Text, TextInput } from '@mantine/core';
import { useDebouncedCallback } from '@mantine/hooks';
import { useMutation } from '@tanstack/react-query';
import { useState } from 'react';

import { searchForecast } from '~/shared/requests/forecast/search';
import { useForecast } from '~/shared/store/forecast';

import { ForecastList } from './forecast-list';

export function HomePage() {
  const [city, setCity] = useState('');
  const [searchedCity, setSearchedCity] = useState<string>('');
  const { forecasts, setForecasts } = useForecast();

  const mutation = useMutation({
    mutationFn: searchForecast,
    onSuccess: (data) => {
      setSearchedCity(data.city);
      setForecasts(data.forecast);
    },
  });

  const handleSeachCity = useDebouncedCallback(async (query: string) => {
    await mutation.mutateAsync(query);
  }, 250);

  const handleChangeCity = (ev: React.ChangeEvent<HTMLInputElement>) => {
    const city = ev.target.value;
    setCity(city);
    if (city) {
      handleSeachCity(city);
    }
  };

  return (
    <Flex mt="xl" w="100%" justify="center">
      <Stack maw={700} w="100%" p="xs">
        <TextInput
          name="city"
          type="text"
          label="City Name"
          placeholder="Enter city"
          value={city}
          onChange={handleChangeCity}
          rightSection={mutation.isPending && <Loader size={20} />}
        />
        {!forecasts.length && (
          <Text mt="xl" fz="h3" fw={500} ta="center">
            Please enter city to revise Weather forecasts
          </Text>
        )}
        {forecasts.length > 0 && (
          <>
            <Text ta="center" fz="h4">
              Weather forecast for{' '}
              <Text fz="lg" fw="bold" span>
                {searchedCity}
              </Text>
            </Text>
            <ForecastList forecasts={forecasts} />
          </>
        )}
      </Stack>
    </Flex>
  );
}
