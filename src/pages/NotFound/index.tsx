import { Box } from '@mantine/core';

export function NotFoundPage() {
  return <Box>404</Box>;
}
