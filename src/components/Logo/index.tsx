import { Avatar, Flex, rem, Text } from '@mantine/core';
import { IconCloud } from '@tabler/icons-react';

export function Logo() {
  return (
    <Flex align="center" justify="center">
      <Avatar variant="filled" radius="md" size="3rem" color="white">
        <IconCloud
          style={{ width: rem(30), height: rem(30) }}
          color="var(--mantine-color-blue-filled)"
        />
      </Avatar>
      <Text tt="uppercase" fw="bold">
        Weather App
      </Text>
    </Flex>
  );
}
