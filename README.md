# Weather Widget Web Application (SPA)

## Overview

This project is a single-page application (SPA) for a weather widget using the following stack:

- **React**
- **React Router**
- **TypeScript**
