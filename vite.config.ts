import path from 'node:path';

import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';

export default defineConfig({
  resolve: {
    alias: {
      '~': path.resolve(__dirname, './src'),
    },
  },
  build: {
    commonjsOptions: {},
    rollupOptions: {
      plugins: [commonjs(), nodeResolve()],
    },
  },
  plugins: [react()],
});
